/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/17/2020
 */

#include <stdio.h>
#include <stdlib.h>

// Definisco una struct adeguata a memorizzare un arco.
typedef struct {
    int source;
    int dest;
} edge;

int inArray(int needle, const int *haystack, int size);
int powerset(int *val, int *sol, int k, edge *edges, int size);
int powersetR(int pos, int *val, int *sol, int k, int count, edge *edges, int size);
int checkSolution(int *sol, int *val, edge* edges, int size, int eSize);
void printSolution(int *sol, int* val, int size);

int main() {
    int *vertexes, *sol, vSize, eSize, vCount = 0, nSol;
    edge *edges;
    char fileName[50 + 1];
    FILE *fp;

    printf("Inserire il nome del file: ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    // Gestione dell'errore relativo al file.
    if (fp == NULL) {
        printf("Errore durante l'apertura del file.");
        return -1;
    }

    // Scansiono il numero di vertici e di archi.
    fscanf(fp, "%d %d", &vSize, &eSize);

    // Alloco dinamicamente i vettori necessari.
    vertexes = (int *) malloc(vSize * sizeof(int));
    sol = (int *) malloc(vSize * sizeof(int));
    edges = (edge *) malloc(eSize * sizeof(edge));

    // Per ogni riga del file (che corrisponde al numero di archi), aggiungo un arco al vettore di archi e un vertice
    // al vettore di vertici.
    for (int i = 0; i < eSize; i++) {
        fscanf(fp, "%d %d", &edges[i].source, &edges[i].dest);

        // Se il vertice sorgente non è contenuto nella lista dei vertici, lo aggiungo.
        if (!inArray(edges[i].source, vertexes, vCount + 1)) {
            vertexes[vCount] = edges[i].source;
            vCount++;
        }

        // Se il vertice di arrivo non è contenuto nella lista dei vertici, lo aggiungo.
        if (!inArray(edges[i].dest, vertexes, vCount + 1)) {
            vertexes[vCount] = edges[i].dest;
            vCount++;
        }
    }

    nSol = powerset(vertexes, sol, vSize, edges, eSize);

    printf("%d soluzioni trovate.", nSol);

    // Dealloco la memoria allocata dinamicamente, nonostante non sia un processo obbligatorio poichè dopo questa
    // istruzione il programma termina.
    free(edges);
    free(sol);
    free(vertexes);

    return 0;
}

// Controlla se il valore `needle` è contenuto nel vettore `haystack` nelle prime `size` posizioni.
int inArray(int needle, const int *haystack, int size)
{
    for (int i = 0; i < size; i ++) {
        if (haystack[i] == needle) {
            return 1;
        }
    }

    return 0;
}

// Funzione wrapper.
int powerset(int *val, int *sol, int k, edge *edges, int size)
{
    int pos = 0, count = 0;

    return powersetR(pos, val, sol, k, count, edges, size);
}

// Calcola tutte le possibili parti dell'insieme di partenza e le stampa se valide.
// La funzione ritorna il numero di soluzioni valide trovate.
int powersetR(int pos, int *val, int *sol, int k, int count, edge *edges, int size)
{
    if (pos >= k) {
        if (checkSolution(sol, val, edges, k, size)) {
            printSolution(sol, val, k);
            return count + 1;
        }
        return count;
    }

    sol[pos] = 0;
    count = powersetR(pos + 1, val, sol, k, count, edges, size);
    sol[pos] = 1;
    count = powersetR(pos + 1, val, sol, k, count, edges, size);

    return count;
}

// Controlla se la soluzione è valida.
int checkSolution(int *sol, int *val, edge* edges, int size, int eSize)
{
    int valid;
    // Per ogni arco
    for (int i = 0; i < eSize; i++) {
        valid = 0;
        // Per ogni posizione della soluzione
        for (int j = 0; j < size; j++) {
            if (sol[j]) {
                // Controllo che una delle due estremità dell'arco sia contenuta.
                if (val[j] == edges[i].source || val[j] == edges[i].dest) {
                    valid = 1;
                    break;
                }
            }
        }

        // Se la soluzione non è valida almeno per un arco, ritorno 0.
        if (!valid) {
            return 0;
        }
    }

    // Ritorno 1 se la soluzione è valida.
    return 1;
}

// Stampa la soluzione con gli elementi originali.
void printSolution(int *sol, int *val, int size)
{
    for (int i = 0; i < size; i++) {
        if (sol[i]) {
            if (i < size - 1) {
                printf("%d, ", val[i]);
            } else {
                printf("%d", val[i]);
            }
        }
    }

    printf("\n");
}