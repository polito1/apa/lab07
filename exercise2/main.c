/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/17/2020
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define DATE_LENGTH 10
#define CODE_LENGTH 5
#define STRING_LENGTH 50
#define MAX_COMMAND_LENGTH 20

typedef struct {
    char codice[CODE_LENGTH + 1];
    char nome[STRING_LENGTH + 1];
    char cognome[STRING_LENGTH + 1];
    char nascita[DATE_LENGTH + 1];
    char via[STRING_LENGTH + 1];
    char citta[STRING_LENGTH + 1];
    int cap;
    struct Item* next;
} Item;

struct node {
    Item val;
    struct node* next;
};

typedef struct {
    char key[STRING_LENGTH + 1];
}Key;

typedef enum {
    leggi_tastiera,
    leggi_file,
    ricerca_codice,
    cancella_codice,
    cancella_date,
    stampa_file,
    fine
};

struct node* leggiFile(struct node *h);
struct node* leggiTastiera(struct node *h);
struct node* ricercaCodice(struct node *h);
struct node* newNode(Item value, struct node *next);
struct node* insertOrder(struct node *h, Item val);
struct node* deleteNode(struct node *h, Key key);
Key getKey(Item val);
int compareKey(Key a, Key b);
int dateCmp(char a[DATE_LENGTH + 1], char b[DATE_LENGTH + 1]);
int dateBetween(char val[DATE_LENGTH + 1], char a[DATE_LENGTH + 1], char b[DATE_LENGTH + 1]);
int leggiComando();
void printItem(Item item);
void stampaFile(struct node *h);

int main() {
    struct node *h = NULL, *tmp, *support;
    int continua = 1;
    char dataInizio[DATE_LENGTH + 1], dataFine[DATE_LENGTH + 1];

    while (continua) {
        switch (leggiComando()) {
            case leggi_tastiera:
                h = leggiTastiera(h);
                break;
            case leggi_file:
                h = leggiFile(h);
                break;
            case ricerca_codice:
                tmp = ricercaCodice(h);
                if (tmp == NULL) {
                    printf("Nessun valore trovato.\n");
                } else {
                    printItem(tmp->val);
                }
                break;
            case cancella_codice:
                tmp = ricercaCodice(h);
                if (tmp != NULL) {
                    h = deleteNode(h, getKey(tmp->val));
                    printf("Ho eliminato: ");
                    printItem(tmp->val);
                    free(tmp);
                } else {
                    printf("Valore non trovato!\n");
                }
                break;
            case cancella_date:
                printf("Inserisci la prima data: ");
                scanf("%s", dataInizio);
                printf("Inserisci la seconda data: ");
                scanf("%s", dataFine);

                // Ci serve una variabile che ogni volta rappresenterà il valore successivo, in modo da poter deallocare
                // `tmp` dopo la sua eliminazione dalla lista.
                support = NULL;
                for (tmp = h; tmp != NULL; tmp = support) {
                    support = tmp->next;
                    if (dateBetween(tmp->val.nascita, dataInizio, dataFine)) {
                        h = deleteNode(h, getKey(tmp->val));
                        printf("Ho eliminato: ");
                        printItem(tmp->val);
                        free(tmp);
                    }
                }
                break;
            case stampa_file:
                stampaFile(h);
                break;
            default:
                continua = 0;
                break;
        }
    }

    return 0;
}

// Legge un comando sfruttando il tipo enum.
int leggiComando()
{
    char comando[MAX_COMMAND_LENGTH + 1];
    char comandi[fine + 1][MAX_COMMAND_LENGTH + 1] = {
            "leggi_tastiera",
            "leggi_file",
            "ricerca_codice",
            "cancella_codice",
            "cancella_date",
            "stampa_file",
            "fine"
    };
    printf("Inserire un comando (leggi_tastiera, leggi_file, ricerca_codice, cancella_codice, cancella_date, stampa_file, fine): ");
    scanf("%s", comando);

    for (int i = 0; i < fine + 1; i++) {
        if (strcmp(comando, comandi[i]) == 0) {
            return i;
        }
    }

    return fine;
}

// Legge un elemento da tastiera e lo inserisce in maniera ordinata nella lista.
struct node* leggiTastiera(struct node *h)
{
    Item value;

    printf("Inserire il dato nel formato: <codice> <nome> <cognome> <data_di_nascita> <via> <citta'> <cap>: ");
    scanf(
            "%s %s %s %s %s %s %d",
          value.codice,
          value.nome,
          value.cognome,
          value.nascita,
          value.via,
          value.citta,
          &value.cap
      );

    h = insertOrder(h, value);

    return h;
}

struct node* leggiFile(struct node *h)
{
    char fileName[STRING_LENGTH];
    FILE *fp;
    Item value;

    printf("Inserire il nome del file: ");
    scanf("%s", fileName);
    fp = fopen(fileName, "r");

    // Controllo eventuali errori del file.
    if (fp == NULL) {
        printf("Errore nel file.");
        exit(-1);
    }

    // Per ogni riga del file creo un nodo e lo inserisco in maniera ordinata nella lista.
    while (!feof(fp)) {
        fscanf(
            fp,
            "%s %s %s %s %s %s %d",
            value.codice,
            value.nome,
            value.cognome,
            value.nascita,
            value.via,
            value.citta,
            &value.cap
        );

        h = insertOrder(h, value);
    }

    // Chiudo il file: non è più utile.
    fclose(fp);

    // Ritorno la testa della lista per aggiornarla.
    return h;
}

struct node* ricercaCodice(struct node *h)
{
    struct node *tmp = h;
    char codice[CODE_LENGTH + 1];

    printf("Inserire il codice da cercare: ");
    scanf("%s", codice);

    while (tmp != NULL && strcmp(codice, tmp->val.codice) != 0) {
        tmp = tmp->next;
    }

    // Se non ho trovato alcun elemento, ritorno NULL, altrimenti ritorno il nodo associato all'elemento di codice
    // cercato.
    return tmp;
}

// Stampa la lista su file.
void stampaFile(struct node *h)
{
    struct node *tmp;
    char fileName[STRING_LENGTH + 1];
    FILE *fp;

    printf("Inserire il nome del file di output: ");
    scanf("%s", fileName);

    fp = fopen(fileName, "w");

    // Controllo eventuale errore del file.
    if (fp == NULL) {
        printf("File error.");
        exit(-1);
    }

    // Per ogni elemento, lo stampo sul file.
    for (tmp = h; tmp != NULL; tmp = tmp->next) {
        fprintf(
                fp,
                "%s, %s, %s, %s, %s, %s, %d\n",
                tmp->val.codice,
                tmp->val.nome,
                tmp->val.cognome,
                tmp->val.nascita,
                tmp->val.via,
                tmp->val.citta,
                tmp->val.cap
        );
    }

    // Chiudo il file: non è più utile.
    fclose(fp);
}

// Compara due date.
int dateCmp(char a[DATE_LENGTH + 1], char b[DATE_LENGTH + 1])
{
    int dayA, monthA, yearA, dayB, monthB, yearB;

    sscanf(a, "%d/%d/%d", &dayA, &monthA, &yearA);
    sscanf(b, "%d/%d/%d", &dayB, &monthB, &yearB);

    if (yearA < yearB) {
        return -1;
    } else if (yearB < yearA){
        return 1;
    }

    if (monthA < monthB) {
        return -1;
    } else if (monthB < monthA) {
        return 1;
    }

    if (dayA < dayB) {
        return -1;
    } else if (dayB < dayA) {
        return 1;
    }

    return 0;
}

// Controlla se la data `val` è compresta tra `a` e `b`.
int dateBetween(char val[DATE_LENGTH + 1], char a[DATE_LENGTH + 1], char b[DATE_LENGTH + 1])
{
    return dateCmp(val, a) >= 0 && dateCmp(val, b) <= 0;
}

// Crea un nuovo nodo e lo ritorna.
struct node* newNode(Item value, struct node *next)
{
    // Alloca lo spazio necessario per un nodo.
    struct node *current = (struct node *) malloc(sizeof(struct node));

    // Controllo eventuale errore di allocazione.
    if (current == NULL) {
        return NULL;
    }

    current->val = value;
    current->next = next;

    // Ritorno il nodo appena creato.
    return current;
}

struct node* insertOrder(struct node *h, Item val)
{
    struct node *x, *p;

    // Se la lista è vuota oppure l'elemento corrente va inserito prima della testa.
    if (h == NULL || dateCmp(h->val.nascita, val.nascita) < 0) {
        return newNode(val, h);
    }

    // Inserisce l'elemento.
    for (
            x = h->next, p = h;
            x != NULL && dateCmp(h->val.nascita, val.nascita) < 0;
            p = x, x = x->next
        );

    p->next = newNode(val, x);

    return h;
}

// Elimina l'elemento con chiave `key`.
// ATTENZIONE: questa funzione non si occupa di deallocare lo spazio inutilizzato, lascia infatti questo
// onere alla funzione chiamante.
struct node* deleteNode(struct node *h, Key key)
{
    struct node *x, *p;
    if (h == NULL) {
        return NULL;
    }

    for (x = h, p = NULL; x != NULL; p = x, x = x->next) {
        if (compareKey(getKey(x->val), key) == 0) {
            if (x == h) {
                h = x->next;
            } else {
                p->next = x->next;
            }
            break;
        }
    }

    return h;
}

// Stampa un item formattato.
void printItem(Item item)
{
    printf(
            "%s, %s, %s, %s, %s, %s, %d\n",
            item.codice,
            item.nome,
            item.cognome,
            item.nascita,
            item.via,
            item.citta,
            item.cap
        );
}

// Ritorna la chiave di un elemento.
Key getKey(Item val)
{
    Key k;
    strcpy(k.key, val.codice);

    return k;
}

// Comparazione di due chiavi.
int compareKey(Key a, Key b)
{
    return strcmp(a.key, b.key);
}